package com.example.ejercicio4_5_6.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/api/holaworld")
    public String helloWorld(){
        return "Hola mundo";
    }
}
