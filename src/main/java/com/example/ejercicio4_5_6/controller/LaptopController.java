package com.example.ejercicio4_5_6.controller;

import com.example.ejercicio4_5_6.entities.Laptop;
import com.example.ejercicio4_5_6.repository.LaptopReposity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Scanner;

@RestController
public class LaptopController {
    // atributos
    private LaptopReposity laptoprepository;
    // constructores

    public LaptopController(LaptopReposity laptoprepository) {
        this.laptoprepository = laptoprepository;
    }


    // CRUD para la entidad laptop

    // buscar todos los Laptop
    // devolvera una lista de laptop

    /**
     * http://localhost:8080/api/laptops
     * @return
     */
    @GetMapping("/api/laptops")
    public List<Laptop> findAll(){

        return laptoprepository.findAll();
    }


    // Buscar un solo laptop por id

    // Crear un nuevo laptop
    @PostMapping("/api/laptops")
    public Laptop create(@RequestBody Laptop laptop){
        return laptoprepository.save(laptop);
    }
    // Actualizar un laptop existente

    // Borrar un laptop

    // Borrar todos los laptop

}
