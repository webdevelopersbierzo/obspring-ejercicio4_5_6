package com.example.ejercicio4_5_6.repository;

import com.example.ejercicio4_5_6.entities.Laptop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LaptopReposity extends JpaRepository<Laptop, Long> {
}
