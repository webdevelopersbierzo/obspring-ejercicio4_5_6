package com.example.ejercicio4_5_6;

import com.example.ejercicio4_5_6.entities.Laptop;
import com.example.ejercicio4_5_6.repository.LaptopReposity;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Ejercicio456Application {

	public static void main(String[] args) {

		ApplicationContext context = SpringApplication.run(Ejercicio456Application.class, args);
		LaptopReposity repository = context.getBean(LaptopReposity.class);


		// verificacion de que este funcionando
		// Creamos un laptop

		Laptop laptop1 = new Laptop(null, "Toshiba", "54jk568m",458.68, true);
		Laptop laptop2 = new Laptop(null, "Samsung", "58loi65",582.68, true);

		// guardamos los laptop creados
		System.out.println("Laptop almacenados " + repository.findAll().size()); // imprimimos los almacenados-> 0

		repository.save(laptop1);
		repository.save(laptop2);

		System.out.println("Laptop almacenados " +repository.findAll().size()); // imprimimos los almacenados -> 2

		// borramos uno

		//repository.deleteById(1L);

		System.out.println("Laptop almacenados " +repository.findAll().size()); // imprimimos los almacenados -> 1


	}

}
